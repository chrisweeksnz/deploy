# deploy

Deployment container with ansible and openstack tools

# Use

The container is designed to be used as an administrative container or within a 
CI pipeline.

### Example Admin Use

Dirty access as the root user of the container:

```shell
docker run \
  -it \
  --rm \
  --net=host \
  -v ${HOME}/.ara:/root/.ara \
  -v ${HOME}/Code:/root/Code \
  registry.gitlab.com/chrisweeksnz/deploy:latest
```

Cleaner use would have either a CI produced container with the user and UID set 
appropriately or the image modified on the client side and saved, so that the 
container can be run in user context:

```shell
docker run \
  -it \
  --rm \
  --net=host \
  -u ${USER} \
  -v ${HOME}/.ara:/home/${USER}/.ara \
  -v ${HOME}/Code:/home/${USER}/Code \
  registry.gitlab.com/chrisweeksnz/deploy:${USER}
```

Under normal circumstances, you will need to also mount volumes containing:
* ansible vault secrets
* ssh private keys

### Example CI Use

Use this image as the source of Gitlab CI jobs:

```yaml
image: registry.gitlab.com/chrisweeksnz/deploy:latest

variables:
  # Gitlab built-in variables found at https://docs.gitlab.com/ce/ci/variables/README.html
  OS_AUTH_URL: replace_me_with_an_openstack_auth_url
  OS_IDENTITY_API_VERSION: replace_me_with_an_openstack_api_version
  OS_REGION_NAME: replace_me_with_an_openstack_region
  OS_PROJECT_NAME: replace_me_with_an_openstack_project
  OS_PROJECT_DOMAIN_NAME: Default
  OS_USERNAME: replace_me_with_an_openstack_username
  OS_USER_DOMAIN_NAME: Default
  # Gitlab Secret Variables:
  # - OS_PASSWORD
  # - ANSIBLE_VAULT_PHRASE

```

Within your CI jobs, ensure to push the contents of the ```.ara/server/``` 
directory somewhere if you wish to retain the data, where not running a 
centralised ara server.


### Example Playbook Run

```shell
# start the container
docker run \
  -it \
  --rm \
  --net=host \
  -v ${HOME}/.ara:/root/.ara \
  -v ${HOME}/Code:/root/Code \
  registry.gitlab.com/chrisweeksnz/deploy:latest

# run a playbook to initialise config directory

cat > ~/test.yml <<'EOF'
---
- hosts: localhost
  tasks:
  - name: gather facts
    setup:
EOF
ansible-playbook ~/test.yml
```


