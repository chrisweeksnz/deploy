FROM ubuntu:rolling

LABEL maintainer="Chris Weeks <chris@weeks.net.nz>" \
      purpose="Consistent deployment environment"

######
# ARA
######
# - https://github.com/ansible-community/ara
# - ~/.ara/server/ansible.sqlite
#   ^^^ needs to be saved on ansible runs for future analysis


ENV PATH /root/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV OS_AUTH_URL ''
ENV OS_IDENTITY_API_VERSION '3'
ENV OS_PROJECT_ID ''
ENV OS_PROJECT_NAME ''
ENV OS_PROJECT_DOMAIN_NAME ''
ENV OS_USERNAME ''
ENV OS_USER_DOMAIN_NAME ''
ENV OS_PASSWORD ''
ENV OS_REGION_NAME ''

RUN apt -y update && \
    apt -y upgrade && \
    DEBIAN_FRONTEND=noninteractive apt -y install \
           bash \
           bind9-host \
           curl \
           expect \
           gawk \
           git \
           iproute2 \
           iputils-ping \
           jq \
           keychain \
           libffi-dev \
           libssl-dev \
           python3 \
           python3-crypto \
           python3-dev \
           python3-pip \
           rsync \
           sed \
           sqlite \
           sudo \
           tzdata \
           vim \
           unzip \
           wget && \
    pip3 install --upgrade \
        ansible \
        ara[server] \
        python-ceilometerclient \
        python-cinderclient \
        python-designateclient \
        python-glanceclient \
        python-heatclient \
        python-keystoneclient \
        python-magnumclient \
        python-neutronclient \
        python-novaclient \
        python-openstackclient \
        python-swiftclient \
        shade && \
    mkdir -p /etc/ansible && \
    wget -P /etc/ansible https://raw.githubusercontent.com/ansible/ansible/devel/examples/ansible.cfg && \
    python3 -m ara.setup.env >> ~/.bashrc

COPY ssh-add-pass /usr/local/bin/ssh-add-pass
COPY ansible_hosts /etc/ansible/hosts
RUN chmod 755 /usr/local/bin/ssh-add-pass
